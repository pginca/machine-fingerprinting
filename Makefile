all: pdf

pdf:
	pdflatex cookies
	bibtex cookies
	pdflatex cookies
	pdflatex cookies
	pdflatex cookies

clean:
	rm -f *.aux *.log *.bbl *.blg *.toc *.lof *.lot *.out
	rm -f head/*.aux head/*.log
	rm -f main/*.aux main/*.log
	rm -f tail/*.aux tail/*.log
	rm -f preamble/*.aux preamble/*.log
